## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Thu Aug 05 2021 18:47:05 GMT+0000 (Coordinated Universal Time)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.2.5|
|**Generation Platform**<br>SAP Business Application Studio|
|**Floorplan Used**<br>Worklist|
|**Service Type**<br>OData Url|
|**Service URL**<br>https://services.odata.org/V2/Northwind/Northwind.svc/
|**Module Name**<br>project1|
|**Application Title**<br>App Title|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|
|**Main Entity**<br>Orders|
|**Navigation Entity**<br>Order_Details|

## project1

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


#### Commits:

20210809 16:10
20210809 16:38
20210809 16:50
20210809 17:18
20211126 17:58
20211126 18:01
20211126 18:15
20211130 13:44
20211206 10:17
20211206 14:43
20211206 15:07
20211206 15:44
20211206 16:03
20211206 16:12
20211206 16:18
20211206 16:22
20211206 16:51
20211206 16:52
20211206 17:25
20211207 08:43
20211207 08:51
20211207 09:08
20211207 09:35
20211213 14:30
20211214 08:46
20211214 09:00
20211214 09:12
20211214 11:00

#### Commits Claro:
20201222 09:17
20201222 09:23